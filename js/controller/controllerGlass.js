// TODO: Xây dựng hàm để render danh sách hình ảnh các loại kính vào bên cột trái 


export let renderGlassList = (arr) => {
    let contentHTML = "" ; 
    arr.forEach(item => {
        let contentIMG =
        `
        <div onclick = "thuKinh('${item.id}')" class = "col-4" >
            <img class = "w-100" src="${item.src}"> 
        </div>
        `
        contentHTML += contentIMG ; 
    })
    document.getElementById('vglassesList').innerHTML = contentHTML ; 
}

// TODO: Xây dựng hàm tìm kiếm vị trí cho trước id kính và danh sách kính 

export let timKiemViTri = (id , arr) => {
    return arr.findIndex(item => item.id == id) ; 
}

export let thayDoiKinh = (viTri , arr) => {
    let element = arr[viTri] ; 
    let contentIMG =
    `
    <img src="${element.virtualImg}"/>
    `
    let contentDIV = 
    `
    <div class = "glass__info">
        <h5>${element.name}-${element.brand} (${element.color})</h5>
        <div class = "glass__about my-3">
            <span style = "display : inline-block ; padding : 5px" class = "glass__price bg-danger mr-2">${element.price}</span> 
            <span class = "text-success">Stocking</span>
        </div>
        <p class = "text-white">${element.description}</p>
        <div class="form-group">
            <input id="identify" type="hidden" value = "${element.id}" />
          </div>
    </div>
    `
    document.getElementById('avatar').innerHTML = contentIMG ; 
    document.getElementById('glassesInfo').innerHTML = contentDIV ; 
    document.getElementById('glassesInfo').style.display = 'block';
}