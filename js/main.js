/**
 * Xây dựng ứng dụng thử kính với các yêu cầu sau đây : 
 * Yêu cầu 1: 
 * Bên trái hiển thị danh sách kính , bên phải hiển thị người mẫu 
 * Giải quyết yêu cầu 1: 
 * Xây dựng hàm để render danh sách hình ảnh các loại kính vào bên cột trái 
 * Yêu cầu 2: 
 * Khi user chọn vào kính nào đó bất kì -> kính đó sẽ gắn vào hình ảnh của cô người mẫu và phía dưới sẽ hiển thị thông tin của kính 
 * Giải quyết yêu cầu 2: 
 * Vậy mỗi một hình ảnh kính sẽ có chức năng -> gắn hàm vào hình ảnh kính -> truyền id định dạng -> từ id tìm ra được vị trí index của kính -> render ra phía bên phải các yêu cầu 
 * Yêu cầu 3: 
 * Khi user click vào nút before hoặc after thì sẽ CHUYỂN QUA các loại kính khác 
 * Giải quyết yêu cầu 3: 
 * Các xử lý sẽ tương đối là giống với việc render ra các câu hỏi đã làm trên lớp 
 */

import { renderGlassList, thayDoiKinh, timKiemViTri } from "./controller/controllerGlass.js";
import { dataGlasses } from "./glassList.js";

// * Render danh sách hình ảnh các loại kính khi user load lại trang 
renderGlassList(dataGlasses) ; 

// TODO: Xây dựng tính năng thử kính khi user click vào hình ảnh kính 

let thuKinh = (idGlass) => {
    let viTri = timKiemViTri(idGlass , dataGlasses) ; 
    thayDoiKinh(viTri , dataGlasses) ; 
}

window.thuKinh = thuKinh ; 

// TODO: Xây dựng tính năng chuyển đổi tới lui các kính với nhau 

let forwardGlass = () => {
    var idEl = document.getElementById('identify').value ; 
    var viTri = timKiemViTri(idEl , dataGlasses) ;
    ++viTri ; 
    thayDoiKinh(viTri , dataGlasses) ;  

}
let backGlass = () => {
    var idEl = document.getElementById('identify').value ; 
    var viTri = timKiemViTri(idEl , dataGlasses) ;
    --viTri ; 
    thayDoiKinh(viTri , dataGlasses) ;  

}

window.forwardGlass = forwardGlass ; 
window.backGlass = backGlass ; 